export interface Bored {

    activity: string;
    accessibility: number;
    type: string;
    participants: number;
    price: number;
    link: string;
    key: string;
    
}

// Converts JSON strings to/from your types
export class Convert {
    public static toBored(json: string): Bored {
        return JSON.parse(json);
    }

    public static boredToJson(value: Bored): string {
        return JSON.stringify(value);
    }
}

