import { TestBed } from '@angular/core/testing';

import { BoringAPIService } from './boring-api.service';

describe('BoringAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BoringAPIService = TestBed.get(BoringAPIService);
    expect(service).toBeTruthy();
  });
});
