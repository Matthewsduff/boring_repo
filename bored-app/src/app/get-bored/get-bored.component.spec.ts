import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetBoredComponent } from './get-bored.component';

describe('GetBoredComponent', () => {
  let component: GetBoredComponent;
  let fixture: ComponentFixture<GetBoredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetBoredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetBoredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
