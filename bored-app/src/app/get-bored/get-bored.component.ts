import { Component, OnInit } from '@angular/core';
import { BoringAPIService } from '../boring-api.service';
import { Bored } from '../models/bored';

@Component({
  selector: 'app-get-bored',
  templateUrl: './get-bored.component.html',
  styleUrls: ['./get-bored.component.scss']
})
export class GetBoredComponent implements OnInit {

  error: any;
  boringResponse: Bored ;
  constructor(private boringService: BoringAPIService) { }

  ngOnInit() {
 //   this.letsGetBored();
  }

  letsGetBored() {
    this.boringService.getBored()
      .subscribe((data: any) =>  {
          this.boringResponse = data;
          console.log(this.boringResponse)
      },
      error => this.error = error);
  }

  clickForBoredom(){
   // console.log('hello')
    this.letsGetBored();
  }

}
