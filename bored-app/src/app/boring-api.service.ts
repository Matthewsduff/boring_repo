import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BoringAPIService {

  boringUrl = 'https://www.boredapi.com/api/';

  constructor(private http: HttpClient) { }

  getBored() {
    return this.http.get(this.boringUrl + 'activity/');
  }

}



